FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

ENV PORT 8080
ENV SENTRY https://29db3879491642f8816c3aa3559b414f@o455442.ingest.sentry.io/5447038
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} -Dsentry.dsn=${SENTRY_DSN} spring-boot:run" ]
